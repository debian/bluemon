LIBRARYPATH ?= /usr/lib
LIBS = -L$(LIBRARYPATH) -lbluetooth `pkg-config --libs dbus-1`
INCLUDES=`pkg-config --cflags dbus-1`
# Where to put stuff on 'make install'?
PREFIX ?= $(DESTDIR)/usr
SBIN    ?= $(PREFIX)/sbin
BIN    ?= $(PREFIX)/bin
MAN    ?= $(PREFIX)/share/man
DOC    ?= $(PREFIX)/share/doc/bluemon
CONFIG ?= $(DESTDIR)/etc/bluemon
DBUSCONFIG ?= $(DESTDIR)/etc/dbus-1/system.d
VERSION = 1.4
DEB_VER = -1
DEB_ARCH ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)
DEB_RELEASE ?= $(shell sh -c '(lsb_release -c || echo unknown) | cut -f2' 2>/dev/null)
   
all: bluemon bluemon-client bluemon-query bluemon.1 bluemon-client.1 bluemon-query.1 bluemon-dbus.7

print-release-name:
	echo $(DEB_RELEASE)

bluemon: bluetooth-monitor.c
	$(CC) $(CFLAGS) $(INCLUDES) $(LIBS) -DVERSION=\"$(VERSION)\" -o $@ $<
bluemon-query: bluemon-query.c
	$(CC) $(CFLAGS) $(INCLUDES) $(LIBS) -DVERSION=\"$(VERSION)\" -o $@ $<
bluemon-client: bluemon-client.c
	$(CC) $(CFLAGS) $(INCLUDES) $(LIBS) -DVERSION=\"$(VERSION)\" -o $@ $<

%.1: %.sgml
	docbook-to-man $< > $@
%.7: %.sgml
	docbook-to-man $< > $@
  
uninstall:
	rm -f $(CONFIG)/bluemon.conf
	rm -f $(DOC)/examples/bluemon.conf
	rm -f $(DOC)/examples/bluemon.init
	rm -f $(DOC)/README
	rm -f $(DOC)/bluemon-dbus.xml
	rm -f $(DOC)/INSTALL
	rm -f $(DOC)/COPYING
	rm -f $(DOC)/changelog
	rm -f $(MAN)/man1/bluemon.1
	rm -f $(MAN)/man1/bluemon-query.1
	rm -f $(MAN)/man1/bluemon-client.1
	rm -f $(MAN)/man7/bluemon-dbus.7
	rm -f $(SBIN)/bluemon
	rm -f $(BIN)/bluemon-query
	rm -f $(BIN)/bluemon-client
   
install: install-bin install-man install-config install-doc install-examples install-readme

install-config: bluemon.default
	install -d $(CONFIG)
	install -m 644 $< $(CONFIG)/bluemon.conf
install-dbus: bluemon-dbus.conf
	install -d $(DBUSCONFIG)
	install -m 644 $< $(DBUSCONFIG)/bluemon.conf
install-readme: README
	install -d $(DOC) 
	install -m 644 README $(DOC)/README
	install -m 644 bluemon-dbus.xml $(DOC)/bluemon-dbus.xml
	install -m 644 changelog $(DOC)/changelog
install-doc: INSTALL COPYING
	install -d $(DOC) 
	install -m 644 INSTALL $(DOC)/INSTALL
	install -m 644 COPYING $(DOC)/COPYING
install-examples: bluemon.default bluemon.init 
	install -d $(DOC) 
	install -d $(DOC)/examples
	install -m 644 bluemon.default $(DOC)/examples/bluemon.conf
	install -m 644 bluemon.init $(DOC)/examples/bluemon.init

install-man: bluemon.1 bluemon-query.1 bluemon-dbus.7 bluemon-client.1
	install -d $(MAN)
	install -d $(MAN)/man1
	install -d $(MAN)/man7
	install -m 644 bluemon.1 $(MAN)/man1/bluemon.1
	install -m 644 bluemon-query.1 $(MAN)/man1/bluemon-query.1
	install -m 644 bluemon-client.1 $(MAN)/man1/bluemon-client.1
	install -m 644 bluemon-dbus.7 $(MAN)/man7/bluemon-dbus.7

install-bin: bluemon bluemon-query bluemon-client
	install -d $(BIN)
	install -d $(SBIN)
	install ./bluemon $(SBIN)/bluemon
	install ./bluemon-query $(BIN)/bluemon-query
	install ./bluemon-client $(BIN)/bluemon-client

clean:
	rm -f bluemon
	rm -f bluemon-query
	rm -f bluemon-client
	rm -f bluemon.1
	rm -f bluemon-query.1
	rm -f bluemon-client.1
	rm -f bluemon-dbus.7
dist-clean: clean
	rm -f .dist
	rm -rf bluemon-$(VERSION)
	rm -f bluemon_$(VERSION).orig.tar.gz

dist: .dist
.dist: bluemon-query.c bluetooth-monitor.c Makefile bluemon-query.sgml bluemon.sgml README INSTALL COPYING bluemon.default bluemon.init bluemon-client.c bluemon-client.sgml bluemon-dbus.sgml bluemon-dbus.xml bluemon-dbus.conf changelog
	rm -rf bluemon-$(VERSION)
	mkdir bluemon-$(VERSION)
	cp -af $^ bluemon-$(VERSION)
	touch .dist

bluemon-$(VERSION): .dist
	
bluemon-$(VERSION).tar.gz: .dist
	tar zcf $@ bluemon-$(VERSION)

