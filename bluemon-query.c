/*
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation;
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDER(S) AND AUTHOR(S) BE LIABLE FOR ANY
 *  CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES 
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF 
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *  ALL LIABILITY, INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PATENTS, 
 *  COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS, RELATING TO USE OF THIS 
 *  SOFTWARE IS DISCLAIMED.
 *
 */

#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

//#define DEBUG

int main(int argc, char ** argv)
{
   DBusMessage* msg;
   DBusMessageIter args;
   DBusConnection* conn;
   DBusError err;
   DBusPendingCall* pending;

   int ret;
   dbus_uint32_t level = -1;
   dbus_bool_t stat = FALSE;
   char* address = "";

   if (2 == argc && 
         NULL != argv[1] && 
         0 == strncmp("--help", argv[1], 7)) {
      printf ("Syntax: bluemon-query [<address>]\n");
      return 1;
   }
   if (2 == argc && NULL != argv[1]) address = argv[1];

   dbus_error_init(&err);
   conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
   if (dbus_error_is_set(&err)) { fprintf(stderr, "Connection Error (%s)\n", err.message); dbus_error_free(&err); }
   if (NULL == conn) { exit(1); }

   msg = dbus_message_new_method_call("cx.ath.matthew.bluemon.server", "/cx/ath/matthew/bluemon/Bluemon", "cx.ath.matthew.bluemon.Bluemon", "Status");
   if (NULL == msg) { fprintf(stderr, "Message Null\n"); exit(1); }
   dbus_message_iter_init_append(msg, &args);
   dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &address); 
   
   dbus_connection_send_with_reply (conn, msg, &pending, -1); // -1 is default timeout
   if (NULL == pending) { fprintf(stderr, "Pending Call Null\n"); exit(1); }
   dbus_connection_flush(conn);
   dbus_message_unref(msg);
   
   dbus_pending_call_block(pending);
   msg = dbus_pending_call_steal_reply(pending);
   if (NULL == msg) { fprintf(stderr, "Reply Null\n"); exit(1); }
   dbus_pending_call_unref(pending);


   if (DBUS_MESSAGE_TYPE_ERROR == dbus_message_get_type(msg)) {
      dbus_message_iter_init(msg, &args);
      dbus_message_iter_get_basic(&args, &address);
      fprintf(stderr, "Error querying status: %s\n", address);
      exit(1);
   }
   dbus_message_iter_init(msg, &args);
   if (dbus_message_iter_get_arg_type(&args) != DBUS_TYPE_INVALID) {
      dbus_message_iter_get_basic(&args, &address);
      dbus_message_iter_next(&args);
   }
   if (dbus_message_iter_get_arg_type(&args) != DBUS_TYPE_INVALID) {
      dbus_message_iter_get_basic(&args, &stat);
      dbus_message_iter_next(&args);
   }
   if (dbus_message_iter_get_arg_type(&args) != DBUS_TYPE_INVALID) {
      dbus_message_iter_get_basic(&args, &level);
   }
   if (stat) 
      printf("%s connected with level %d\n", address, level);
   else if (strncmp(address, "", 1))
      printf("nothing connected\n", address);
   else
      printf("%s not connected\n", address);
   
   dbus_message_unref(msg);   
   dbus_connection_close(conn);
 }
