<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Matthew</firstname>">
  <!ENTITY dhsurname   "<surname>Johnson</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>November  1, 2005</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>7</manvolnum>">
  <!ENTITY dhemail     "<email>&lt;debian@matthew.ath.cx&gt;</email>">
  <!ENTITY dhusername  "Matthew Johnson">
  <!ENTITY dhucpackage "<refentrytitle>BLUEMON DBUS INTERFACES</refentrytitle>">
  <!ENTITY dhpackage   "bluemon">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2005</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>DBUS interface to bluemon(1)</refpurpose>
  </refnamediv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the DBUS interface to bluemon(1)</para>

  </refsect1>
  <refsect1>
    <title>INTERFACE</title>

    <para>
      Bluemon exposes 2 signals and 1 method. The bluemon process is registered
      on the system bus as <command>cx.ath.matthew.bluemon.server</command>. The
      exposed object is <command>/cx/ath/matthew/bluemon/Bluemon</command> and the
      exposed interface is <command>cx.ath.matthew.bluemon.Bluemon</command>. The
      signal interface is <command>cx.ath.matthew.bluemon.ProximitySignal</command>
      and have the names <command>Connect</command> and <command>Disconnect</command>
      which both have 1 parameter, which is the string representation of the
      bluetooth device ID which has been connected or disconnected. The method
      has the signature <command>Status(IN STRING address, OUT STRING address,
            OUT BOOLEAN status, OUT UINT32 level)</command>. The in parameter may
      be blank in which case the first connected device will be returned.  If
      status is true (device is connected) then the address and level will
      correspond to the device address and the reported link quality. Otherwise
      they will set to the address asked for and -1 respectively.
    </para>
    <para>
      This information is also available as an XML protocol
      description in /usr/share/doc/bluemon/bluemon-dbus.xml
    </para>

  </refsect1>
  <refsect1>
    <title>SEE ALSO</title>

    <para>bluemon (1), bluemon-client(1), bluemon-query(1).</para>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 any 
	  later version published by the Free Software Foundation.
    </para>
	<para>
	  On Debian systems, the complete text of the GNU General Public
	  License can be found in /usr/share/common-licenses/GPL.
	</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


