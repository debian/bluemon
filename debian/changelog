bluemon (1.4-10) unstable; urgency=medium

  * QA upload.

  * Added d/gbp.conf to describe branch layout.
  * Updated vcs in d/control to Salsa.
  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.5.1 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Adjusted d/rules to install dbus config in /usr/.
  * Pre-depend on ${misc:Pre-Depends} for correct installation ordering.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 19 May 2024 14:48:14 +0200

bluemon (1.4-9) unstable; urgency=medium

  * QA upload.
  * Fix FTCBFS, thanks to Helmut Grohne.  (Closes: #900194)
  * Fix building with -Werror=implicit-function-declaration.
    (Closes: #1066330)

 -- Andreas Beckmann <anbe@debian.org>  Thu, 04 Apr 2024 17:55:45 +0200

bluemon (1.4-8) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper >= 10' to
        'debhelper-compat = 13'.
  * debian/control:
      - Added 'Rules-Requires-Root: no' in source stanza.
      - Bumped Standards-Version to 4.5.1.
  * debian/copyright:
      - Updated Format field from 'http' to 'https'.
      - Updated packaging copyright years.
  * debian/rules: removed unnecessary dh argument '--parallel'.
  * debian/watch: updated to version 4.

 -- Thiago Andrade Marques <andrade@debian.org>  Fri, 04 Dec 2020 12:49:09 -0300

bluemon (1.4-7) unstable; urgency=medium

  * QA upload.
  * Convert to Source-Format 3.0 (quilt)
  * debian/control, debian/compat:
    - change debhelper-compat level to 10
  * debian/control:
    - set Maintainer to QA group, package is orphaned (#762537)
    - add dependency on lsb-base for init script
    - run wrap-and-sort -sa
    - declare policy-compliance for Policy 3.9.8
    - Add a homepage field
  * debian/rules:
    - use dh
  * debian/prerm+preinst:
    - Remove prerm file, was only needed for upgrades from version 1.3-1.2,
      which no longer is in any supported Debian release
    - Remove preinst file, debhelper does the right thing automatically
  * debian/dirs:
    - Remove, not needed
  * debian/init:
    - Add $remote_fs to Required-Start/-Stop
    - Also stop at runlevel 1
    - Don't start daemon on restart action if disabled in /etc/defaults/bluemon
      (Closes: #679109)
  * debian/copyright:
    - Add a machine-readable copyright-file
  * debian/patches:
    - add-cppflags-hardening.patch: Add CPPFLAGS and LDFLAGS to pass hardening
      options to the compiler. This also fixes build with binutils-gold
      (Closes: #612181)
    - add clang-ftbfs.diff to Fix FTBFS with clang:
      Fixed the non-void function should return a value in
      bluetooth-monitor.c (Closes: #740036)
      Patch from Arthur Marble <arthur@info9.net>

 -- Andreas Moog <andreas.moog@warperbbs.de>  Fri, 26 May 2017 19:42:43 +0200

bluemon (1.4-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Bump debhelper compatibility level to 9 (Closes: #817379)

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 13 Aug 2016 12:33:20 -0300

bluemon (1.4-6) unstable; urgency=low

  * Change dependency bluez-utils => bluez (Closes: #534315)
  * Change build-dep libbluetooth2-dev => libbluetooth-dev (Closes: #530353)
  * Dummy closes to mark bug which was fixed ages ago an fixed here (Closes: #399021)

 -- Matthew Johnson <mjj29@debian.org>  Thu, 25 Jun 2009 08:21:00 +0100

bluemon (1.4-5) unstable; urgency=high

  * Add fix-system-bus-permissions.diff so that it works
    with the fixed dbus (Closes: #510628)
  * Urgency high because it blocks #503532
  * Adding a dependency on quilt to apply the patch

 -- Matthew Johnson <mjj29@debian.org>  Sat, 03 Jan 2009 21:31:53 +0000

bluemon (1.4-4) unstable; urgency=low

  * Bump Standards-Version

 -- Matthew Johnson <mjj29@debian.org>  Tue, 05 Feb 2008 09:28:01 +0000

bluemon (1.4-3) unstable; urgency=low

  * Update copyright
  * Ensure init.d-script-missing-lsb-section fix is actually included this
    time...
  * Remove superfluous files.
  * Update maintainer address

 -- Matthew Johnson <mjj29@debian.org>  Wed, 05 Dec 2007 10:24:56 +0000

bluemon (1.4-2) unstable; urgency=low

  * Fix init.d-script-missing-lsb-section
  * Use lsb-functions in init script

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon, 27 Nov 2006 13:19:41 +0000

bluemon (1.4-1) unstable; urgency=low

  * New Upstream Release
  * Don't enable by default. (Closes: #392260)
    Patch from Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
  * Starting with default config file confirmed working
    (Closes: #359240)

 -- Matthew Johnson <debian@matthew.ath.cx>  Tue, 14 Nov 2006 13:36:50 +0000

bluemon (1.3-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Don't fail when bluemon is already stopped. (Closes: #387433)

 -- Arjan Oosting <arjanoosting@home.nl>  Fri, 29 Sep 2006 20:18:47 +0200
bluemon (1.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix dependency from libbluetooth1 to libbluetooth2. (closes: #376875)
  * Bump standards version

 -- Julien Danjou <acid@debian.org>  Tue, 11 Jul 2006 13:43:16 +0200
bluemon (1.3-1) unstable; urgency=low

  * New Upstream Release:
  - bug fixes
  Closes: #348693: manpage misplaced
  Closes: #349791: Does not have DBus privileges to start
  * Clarify description
  Closes: #348691: bluemon description could mention dbus

 -- Matthew Johnson <debian@matthew.ath.cx>  Wed, 25 Jan 2006 10:23:50 +0000

bluemon (1.2-1) unstable; urgency=low

  * New Upstream Release:
  - Added support for monitoring multiple devices, disabling of link quality
    monitoring and defaults for threshold and interval

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon,  9 Jan 2006 14:50:24 +0000

bluemon (1.1-1) unstable; urgency=low

  * New Upstream Release:
  - DBUS integration, Separate client and server binaries,
    fix for disconnects

 -- matthew johnson <debian@matthew.ath.cx>  Tue,  1 Nov 2005 14:20:04 +0000

bluemon (1.0-1) unstable; urgency=low

  * Released version 1.0, documentation and build fixes.

 -- Matthew Johnson <debian@matthew.ath.cx>  Tue, 18 Oct 2005 12:45:24 +0100

bluemon (0.02-1) unstable; urgency=low

  * New Upstream Release:
  - Many fixes and upgrades, Query infrastructure.

 -- Matthew Johnson <debian@matthew.ath.cx>  Wed, 13 Apr 2005 08:40:42 +0100

bluemon (0.01-1) unstable; urgency=low

  * Initial Release.

 -- Matthew Johnson <debian@matthew.ath.cx>  Tue,  9 Nov 2004 13:28:09 +0000
