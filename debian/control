Source: bluemon
Section: net
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Homepage: http://www.matthew.ath.cx/projects/bluemon/
Build-Depends: debhelper-compat (= 13),
               docbook-to-man,
               dpkg-dev (>= 1.16.1~),
               libbluetooth-dev,
               libdbus-1-dev (>= 0.60),
               pkgconf,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/bluemon
Vcs-Git: https://salsa.debian.org/debian/bluemon.git

Package: bluemon
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: bluez,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Activate or deactivate programs based on Bluetooth link quality
 BlueMon monitors the quality of the link to a Bluetooth device and can start
 or stop programs when the threshold drops below a certain value. This can be
 used to perform actions like locking the terminal when you walk away from it.
 .
 Bluemon uses the DBus system bus to alert other applications to the presence
 of Bluetooth devices. A user binary can then start or stop programs when such
 signals are received.
